package at.meself.demo.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * https://blog.mimacom.com/using-the-openapi-generator-for-spring-boot/
 */

@SpringBootApplication
public class SwaggerSpringBootOpenApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(SwaggerSpringBootOpenApiApplication.class, args);
    }

}
