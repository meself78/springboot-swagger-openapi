package at.meself.demo.spring.controller;

import at.meself.demo.spring.api.SearchDeveloperApi;
import at.meself.demo.spring.model.DeveloperItemDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@RestController
public class HelloTeamController implements SearchDeveloperApi {

    @Override
    public ResponseEntity<Void> addDeveloper(DeveloperItemDto developerItemDto) {
        return new ResponseEntity<>(HttpStatus.OK);
    }


    @Override
    public ResponseEntity<List<DeveloperItemDto>> searchDeveloper(String searchString, Integer skip, Integer limit) {
        return ResponseEntity.ok(Arrays.asList(new DeveloperItemDto[]{new DeveloperItemDto().id(UUID.randomUUID()).name(searchString)})
        );
    }



}
